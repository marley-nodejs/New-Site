var mongoose = require('mongoose'),
	connect  = require('./connect').gestion;
	
db = mongoose.createConnection(connect);

db.model('product' ,new mongoose.Schema({
	name     : String,
	prix     : Number,
	stock    : Number,
	category : String,
	local    : String,
	image    : {type : String, default : '/images/banner-aius-small.png'}
})); 

db.model('category' ,new mongoose.Schema({
	name     : String,
	products : Array,
	local    : String
}));

db.model('vente' , new mongoose.Schema({
	nomVendeur : String,
	listeVente : Array,
	caisse     : Number,
	local      : String,
	date       : { type : Date, default : Date.now }
}));

db.model('reservationCroc',  new mongoose.Schema({
	acheteur	: String,
	menu		: Number,
	croc1		: {nature : Boolean, jambon : Boolean, tomate: Boolean},
	croc2		: {nature : Boolean, jambon : Boolean, tomate: Boolean},
	prix		: Number,
	payer		: { type : Boolean, default : false},
	servi		: { type : Boolean, default : false},
	heure		: Number,
	date		: { type : Date, default : Date.now }
}));

db.model('reservationCable',new mongoose.Schema({
	acheteur : String,
	longeur  : Number,
	croise   : Boolean,
	prix     : Number,
	payer    : { type : Boolean, default : false},
	pret     : { type : Boolean, default : false},
	date     : { type : Date, default : Date.now }
}));
	
module.exports = db;
