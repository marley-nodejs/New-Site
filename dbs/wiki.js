var mongoose = require('mongoose'),
	connect  = require('./connect').wiki;
	
db = mongoose.createConnection(connect);

db.model('page' , new mongoose.Schema({
	title   : String,
	ownerId : String,
	body    : String,
	date    : { type: Date, default: Date.now },
	tags    : [String]  
}));

module.exports = db;
