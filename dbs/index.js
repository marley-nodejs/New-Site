module.exports = {
	site    : require('./site'),
	gestion : require('./gestion'),
	wiki    : require('./wiki'),
	file    : require('./file')
}
