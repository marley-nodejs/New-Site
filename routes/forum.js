var express    = require('express'),
	router     = express.Router(),
	mongoose   = require('../dbs').site,
	sha1       = require('sha1'),
	md5        = require('js-md5'),
	htmlEncode = require('../html_encode');

/* GET forum page  1. */
router.get('/', function(req, res, next) {
	mongoose.model('section').find({groupe : {$lte : req.session.account.groupe}},function (err, sections){ //on cherche les sections
		if(sections == null){
			res.render('forum', { sections : [], session : req.session });
		}
		else {
			
			var idThemes = []; // on fait la liste des id des themes
			for (i = 0 ; i<sections.length ;i++){
				idThemes = idThemes.concat(sections[i].themes);
			}
			
			mongoose.model('theme').find({'_id' : { $in : idThemes } },function (err, themes){ //on cherche les themes
				//+
				for (i = 0 ; i<sections.length ;i++){// on tri les themes par section
					var listTheme = [];
					var nb = sections[i].themes.length;
					var j = 0;
					while (nb > 0){
						if (themes[j].section == sections[i].name) {
							listTheme = listTheme.concat(themes.splice(j,1));
							nb--;
						}
						else 
							j++;
					}
					sections[i].themes = listTheme;
				}
				res.render('forum', { sections : sections, session : req.session });
			});
		}
	});	
});

/*theme pages*/
router.use('/:idSection/:idTheme',function(req,res,next){
	var err = new Error('Not Found');
	err.status	 = 404;
	mongoose.model('section').findOne({name : req.params.idSection},function (err, section){// on verifie que la section existe
		if(section == null || req.session.account.groupe < section.groupe){ // et que l'utilisateur a acces a la section
			next(err);
		}
		else {
			mongoose.model('theme').findOne({section : section.name , name:req.params.idTheme},function (err, theme){// on verifie que le theme existe
				if(theme == null){
					next(err);
				}	 
				else {
					req.section = section;
					req.theme = theme;
					next();
				}
			});
		}
	});
});

/* GET forum page  2. */
router.get('/:idSection/:idTheme', function(req, res, next) {
	mongoose.model('topic').find({'_id' : { $in : req.theme.topics }}).sort({lastPost : -1}).exec(function (err, topics){ //on cherche les topics du theme
		res.render('forum_theme', { topics : topics, theme : req.theme, section : req.section, session : req.session});
	});	
});

/* GET forum page post topic. */
router.get('/:idSection/:idTheme/new', function(req, res, next) {
	if (req.session.account.groupe >= 20)
		res.render('forum_post_new', { session : req.session , idSection : req.params.idSection, idTheme : req.params.idTheme });
	else
		next();
});

/* POST forum page post. */
router.post('/:idSection/:idTheme/new', function(req, res, next) {
	if(req.session.account.groupe >= 20 && typeof req.body.InputName == "string" && typeof req.body.InputMessage == "string" ){
		mongoose.model('topic').findOne({name : req.body.InputName, theme : req.params.idTheme , section : req.params.idSection}, function(err, topic){
			if(topic == null && req.body.InputName != "new"){ //on cree un nouveau topic si il n'exite pas deja
				var post = new (mongoose.model('post'));
				post.pseudo = req.session.account.pseudo;
				post.message = htmlEncode(req.body.InputMessage);
				post.topic = req.body.InputName;
				post.theme = req.params.idTheme;
				post.section = req.params.idSection;
				
				post.save(function(err, post){
					
					topic = new (mongoose.model('topic'));
					topic.name = req.body.InputName;
					topic.theme = req.params.idTheme;
					topic.section = req.params.idSection;
					topic.pseudoCreateur = req.session.account.pseudo;
					topic.posts.push(post.id);
					
					topic.save(function(err, topic){
						mongoose.model('theme').update({name : req.theme.name},{$push : { topics :topic.id }},function(err){
							mongoose.model('account').update({pseudo :req.session.account.pseudo },{$push:{posts : post.id}},function(){});
							res.redirect('/forum/'+req.params.idSection+'/'+req.params.idTheme+'/'+topic.name);
						});
						
					});
				});
			}
			else {
				res.render('forum_post', {err : "Nom déja utiliser" , idSection : req.params.idSection, idTheme : req.params.idTheme, idTopic : req.params.idTopic, session : req.session});
			}
		});
	}
	else 
		next();
});

/*topic pages*/
router.use('/:idSection/:idTheme/:idTopic',function(req, res, next) {
	mongoose.model('topic').findOne({name : req.params.idTopic, theme : req.params.idTheme , section : req.params.idSection}, function(err, topic){
		if(topic == null){
			var err = new Error('Not Found');
			err.status	 = 404;
			next(err);
		}
		else {
			req.postPage = 10;
			req.maxPage = Math.floor((topic.posts.length-2)/req.postPage )+1;
			req.page = parseInt(req.query.page);

			if (isNaN(req.page)) // on test la page
				req.page = 0;
			else if (req.page > req.maxPage )
				req.page = req.maxPage-1;
			else
				req.page --;
			req.topic = topic;
			next();
		}
	});
});

/* GET forum page  3. */
router.get('/:idSection/:idTheme/:idTopic/', function(req, res, next) {

	var firstPost = req.topic.posts.shift();

	req.topic.posts = req.topic.posts.slice(req.page*req.postPage , Math.min(req.page*req.postPage+req.postPage,req.topic.posts.length)); // on prend les post de la page qu'on veut
	
	mongoose.model('post').findOne({'_id' : firstPost},function(err, firstPost){ //on cherche le premier post,
		mongoose.model('account').findOne({pseudo : firstPost.pseudo}, function(err, firstAccount){// son account,
			mongoose.model('post').find({'_id' : { $in : req.topic.posts }},function(err, posts){// les autres posts,
				var idAccounts  = [],
					postsLength = posts.length;
				for (i = 0 ; i<postsLength ;i++){
					idAccounts = idAccounts.concat(posts[i].pseudo); // on cree un array contenant les pseudos de tout les posts
				}
				mongoose.model('account').find({pseudo : {$in : idAccounts}},{pseudo : 1, champDescriptif : 1, imProfil : 1, "_id" : 0},function(err , accounts){// et les autres accounts
					
					accounts = accounts.concat({pseudo : "",champDescriptif : "", imProfi :""}); //on enregistre un utilisateur pour les posts suprimer
					//+
					for (i = 0 ; i<postsLength ;i++){
						var j=-1;
						while (posts[i].pseudo != accounts[++j].pseudo); // on cherche le accounts corespondant au post
						posts[i].account = accounts[j];
					}
					res.render('forum_post', { firstPost : firstPost, firstAccount : firstAccount, posts : posts, topic : req.topic,  page : req.page+1, maxPage : req.maxPage, session : req.session });
				});
			});
		});
	});
});

/* POST forum page  3. */
router.post('/:idSection/:idTheme/:idTopic', function(req, res, next) {
	if(req.session.account.groupe >= 20 && typeof req.body.message === "string" && !(/^([\n\f\s\r\t\v])+$/).test(req.body.message)){
		post = new (mongoose.model('post'));
		post.section = req.params.idSection;
		post.theme = req.params.idTheme;
		post.topic = req.params.idTopic;
		post.message = htmlEncode(req.body.message);
		post.pseudo = req.session.account.pseudo;
		
		post.save(function(err, post){							
			mongoose.model('topic').update({name : req.params.idTopic},{$push:{posts : post.id},$set : {lastPost : post.date}},function(){
				mongoose.model('account').update({pseudo :req.session.account.pseudo },{$push:{posts : post.id}},function(){
					res.redirect('/forum/'+req.params.idSection+'/'+req.params.idTheme+'/'+req.params.idTopic+'?page='+(req.page+1));
				});}
			);
		});
	}
	else 
		next();
});

router.get('/:idSection/:idTheme/:idTopic/resolue', function(req, res, next) {
	if(req.session.account.pseudo == req.topic.pseudoCreateur){
		mongoose.model('topic').update({name : req.params.idTopic, theme : req.params.idTheme , section : req.params.idSection},{$set : {isResolue : true}},function(err, topic){});
	}
	res.redirect('/forum/'+req.params.idSection+'/'+req.params.idTheme+'/'+req.params.idTopic+'?page='+(req.page+1));
});

/* Modifier post*/
router.use('/:idSection/:idTheme/:idTopic/edit/:idPost', function(req, res, next) {
	
	mongoose.model('post').findOne({_id : req.params.idPost}, function(err, post){
		if(post != null && (req.session.account.groupe >= 40 || req.session.account.pseudo == post.pseudo)){
			req.post = post;
			next()
		}
		else {
			var err = new Error('Not Found');
			err.status = 404;
			next(err);
		}
	});
});

/* GET forum page edit post. */
router.get('/:idSection/:idTheme/:idTopic/edit/:idPost', function(req, res, next) {
	res.render('forum_edit_post', {post : req.post, session : req.session });
});

/* POST forum page edit post. */
router.post('/:idSection/:idTheme/:idTopic/edit/:idPost', function(req, res, next) {
	mongoose.model('post').update({_id : req.params.idPost},{$set : { message : htmlEncode(req.body.message)}},function(){;
		res.redirect('/forum/'+req.params.idSection+'/'+req.params.idTheme+'/'+req.params.idTopic+'?page='+(req.page+1));
	});
});

/* GET forum page suprr post. */
router.get('/:idSection/:idTheme/:idTopic/suppr/:idPost', function(req, res, next) {
	if(req.session.account.groupe >= 40 .model('post').model('post')){
		mongoose.model('post').update({_id : req.params.idPost},{ $set : { message : 'Post supprimé', pseudo : ''}},function(){;
			res.redirect('/forum/'+req.params.idSection+'/'+req.params.idTheme+'/'+req.params.idTopic);
		});
	}
	else
		res.redirect('/forum/'+req.params.idSection+'/'+req.params.idTheme+'/'+req.params.idTopic)
});

module.exports = router;
