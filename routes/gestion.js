var express = require('express');
var router = express.Router();
var mongoose = require('../dbs').gestion;
var sha1 = require('sha1');
var md5 = require('js-md5');
var multer = require('multer');
var fs = require('fs');

var err404 = new Error('Not Found');
	err404.status	 = 404;

router.use(function(req, res, next) {
	if (req.session.account.groupe >= 50){
		if (req.query.local === undefined || (req.query.local != 'esplanade' && req.query.local != 'illkirch'))
			req.query.local = 'esplanade';
		next();
	}
	else
		next(err404);
});

/* GET gestion cash desk page. */
router.get('/cashDesk', function(req, res, next) {
	var productModel = mongoose.model('product');
	var categoryModel = mongoose.model('category');
	categoryModel.find({local : req.query.local}, function(err, categories){
		productModel.find({local : req.query.local} , function(err, products){
			
			for (i = 0 ; i<categories.length ;i++){
				var listProducts = [];
				var nb = categories[i].products.length;
				var j = 0;
				while (nb > 0){
					if (products[j].category == categories[i].name) {
						listProducts = listProducts.concat(products.splice(j,1));
						nb--;
					}
					else {
						j++;
					}
				}
				categories[i].products = listProducts;
			}
			res.render('gestion_cashDesk', { err: '' ,local : req.query.local, session : req.session, categories : categories});
		});
	});
});

/* POST gestion cash desk page. */
router.post('/cashDesk', function(req, res, next) {
	if(req.body.listeVente !== undefined && req.body.caisse !== undefined){
		var listeVente = JSON.parse(req.body.listeVente);
		var listeVente2 = [] ;
		var productModel = mongoose.model('product');
		var venteModel   = mongoose.model('vente');
		
		for (i=0; i < listeVente.length; i++ ){
			listeVente2[i] = {name : listeVente[i].name , category : listeVente[i].category,local : req.query.local};
		}
		productModel.find({$or: listeVente2,local : req.query.local},function(err,products){
			if(products.length == listeVente.length){
				var vente = new venteModel;
				vente.listeVente = listeVente;
				vente.nomVendeur = req.session.account.pseudo;
				vente.caisse = req.body.caisse;
				vente.local = req.query.local;
				vente.save(function(err, vente){
					nbAFaire = listeVente.length;
					nbFait = 0
					while (listeVente.length > 0){
						var elVente = listeVente.pop();
						productModel.update({name: elVente.name, category : elVente.category,local : req.query.local},{$inc : {stock : -elVente.nb}},function(err, product){
							if (err) { throw err; }
							nbFait++
							if (nbFait == nbAFaire)
								res.send('OK');
						});
					}
				});
			}
		});
	}
});	

/* GET gestion reservation page. */
router.get('/reservation', function(req, res, next) {
	res.render('gestion_reservation', {local : req.query.local, session : req.session});
});

/* POST gestion reservation . */
router.post('/reservation', function(req, res, next) {
	mongoose.model('reservationCroc').find(null).sort({heure : 1}).exec( function(err , reservs){
		res.send(JSON.stringify(reservs));
	});
});


/* GET gestion reservation payer. */
router.get('/reservation/payer', function(req, res, next) {
	mongoose.model('reservationCroc').update({'_id' : req.query.id},{$set:{payer : true}}, function(err , reserv){
		res.send('OK');
	});
});

/* GET gestion reservation servi. */
router.get('/reservation/servi', function(req, res, next) {
	mongoose.model('reservationCroc').update({'_id' : req.query.id},{$set:{servi : true}}, function(err , reserv){
		res.send('OK');
	});
});

/* GET gestion reservation annuler. */
router.get('/reservation/annuler', function(req, res, next) {
	mongoose.model('reservationCroc').remove({'_id' : req.query.id}, function(err , reserv){
		res.send('OK');
	});
});

/* POST gestion reservation Cable . */
router.post('/reservation/Cable', function(req, res, next) {
	mongoose.model('reservationCable').find(null, function(err , reservs){
		res.send(JSON.stringify(reservs));
	});
});

/* GET gestion reservation cable payer. */
router.get('/reservation/cable/payer', function(req, res, next) {
	mongoose.model('reservationCable').update({'_id' : req.query.id},{$set:{payer : true}}, function(err , reserv){
		res.send('OK');
	});
});

/* GET gestion reservation cable pret. */
router.get('/reservation/cable/pret', function(req, res, next) {
	mongoose.model('reservationCable').update({'_id' : req.query.id},{$set:{pret : true}}, function(err , reserv){
		res.send('OK');
	});

});

/* GET gestion reservation cable annuler. */
router.get('/reservation/cable/annuler', function(req, res, next) {
	mongoose.model('reservationCable').remove({'_id' : req.query.id}, function(err , reserv){
		res.send('OK');
	});

});
router.use(function(req, res, next) {
	if (req.session.account.groupe >= 70)
		next();
	else
	{
		var err = new Error('Not Found');
		err.status	 = 404;
		next(err);
	}
});

/* GET gestion reset */
router.get('/reset', function(req, res, next) {
	var productModel = mongoose.model('product');
	productModel.update({local : req.query.local} ,{stock:0},{multi : true}, function(err, products){
		res.redirect('/gestion/product?local='+req.query.local);
	});
});

router.use('/product',multer({
	dest: './public/productsImages/',
	rename: function (fieldname, filename) {
		return Date.now() + filename.replace(/\W+/g, '-').toLowerCase()
	}
}));

/* GET gestion products page. */
router.get('/product', function(req, res, next) {
	var productModel = mongoose.model('product');
	var categoryModel = mongoose.model('category');

	categoryModel.find({local : req.query.local},function (err, categories){
		if(categories != null){
			productModel.find({local : req.query.local} , function(err, products){
				res.render('gestion_products', { err: '' ,local : req.query.local, session : req.session, products : products, categories : categories});
			});
		}
		else
			res.render('gestion_products', { sections : [], local : req.query.local, session : req.session, products : products, categories : categories});
	});
});


/* POST gestion products page. */
router.post('/product', function(req, res, next) {
	var productModel = mongoose.model('product');
	var categoryModel = mongoose.model('category');
	categoryModel.findOne({ name : req.body.category, local : req.query.local}, function(err, category){
		if(category == null)
			next();
		else {
			productModel.findOne({name : req.body.product, category: req.body.category, local : req.query.local}, function(err, product){
				if(product == null){
					product = new productModel;
					product.name = req.body.product;
					product.prix = req.body.prix;
					product.stock = 0;
					product.category = req.body.category;
					product.local = req.query.local;
					if (req.files.file !== undefined)
						product.image = '/productsImages/'+req.files.file.name;
					
					product.save(function (err, product){
						categoryModel.update({name: req.body.category, local : req.query.local},{$push : {products : product.id}}, function(){		
							res.redirect('/gestion/product?local='+req.query.local);
						});
					});
				}
				else 
					next();//add err
			});
		}
	});
});


/* GET gestion product option page. */
router.get('/product/:idProduct', function(req, res, next) {
	var productModel = mongoose.model('product');
	productModel.findOne({name : req.params.idProduct, local : req.query.local}, function (err, product){
		if (err) { throw err; }
		if (product==null) 
			next();
		else
			res.render('gestion_products_option', { err: '' ,local : req.query.local, session : req.session, product :  product});
	});
});


/* POST gestion products option page. */
router.post('/product/:idProduct', function(req, res, next) {
	var productModel = mongoose.model('product');
	var categoryModel = mongoose.model('category');
	productModel.findOne({name : req.params.idProduct, local : req.query.local}, function(err, product){
		if(product == null)
			next();
		else {
			product.name = req.body.name;
			product.prix = req.body.prix;
			product.stock = req.body.stock;
			if (req.files.file !== undefined){
				if (product.image != '/images/banner-aius-small.png')
					fs.unlink('public'+product.image);
				product.image = '/productsImages/'+req.files.file.name;
			}
			if(product.category == req.body.category){
				product.save(function (err, product){
					res.redirect('/gestion/product/?local='+req.query.local);
				});
			}
			else {
				categoryModel.findOne({name : req.body.category, local : req.query.local}, function(err, category){
					if(category==null)
						next();
					else {
						category.products.push(product.id);
						categoryModel.update({name: req.body.category, local : req.query.local},{$set : {products : category.products}}, function(){ //*
							categoryModel.findOne({name : product.category, local : req.query.local}, function(err, category){
								
								category.products.splice(category.products.indexOf(product.id),1);
								categoryModel.update({name:  product.category, local : req.query.local},{$set : {products : category.products}}, function(){
									
									product.category = req.body.category
									product.save(function (err, product){
										res.redirect('/gestion/product/?local='+req.query.local);
									});
									
								});
							});
						});
					}
				});
			}
		}
	});
});

/* GET gestion product suprr. */
router.get('/product/:idProduct/suprr', function(req, res, next) {
	var productModel = mongoose.model('product');
	productModel.findOne({name : req.params.idProduct, local : req.query.local}, function (err, product){
		if (err) { throw err; }
		if (product != null){
			productModel.remove({name : req.params.idProduct, local : req.query.local},function(){});
			if (product.image != '/images/banner-aius-small.png')
				fs.unlink('public'+product.image);
			mongoose.model('category').update({name:  product.category,local : req.query.local},{$pull : {products : product.id}},function(){});
			res.redirect('/gestion/product/?local='+req.query.local);
		}
		else 
			next()
	});
});

/* GET gestion category page. */
router.get('/category', function(req, res, next) {
	var categoryModel = mongoose.model('category');
	categoryModel.find({local : req.query.local}, function(err, categories){
		res.render('gestion_category', { err: '' , local : req.query.local, session : req.session, categories :  categories});
	});
});

/* POST gestion category page. */
router.post('/category', function(req, res, next) {
	var categoryModel = mongoose.model('category');
	categoryModel.findOne({name : req.body.category, local : req.query.local}, function(err, category){
		if(category == null){
			category = new categoryModel;
			category.name = req.body.category;
			category.local = req.query.local;
			category.save(function (err, category){
				res.redirect('/gestion/category?local='+req.query.local);
			});
		}
		else 
			next();//ad err
	});
});

router.use('/category/:idCategory', function(req, res, next) {
	mongoose.model('category').findOne({name : req.params.idCategory, local : req.query.local}, function (err, category){
		if (category == null) 
			next(err404);
		else{
			req.category = category
			next()
		}
	});
});

/* GET gestion category option page. */
router.get('/category/:idCategory', function(req, res, next) {
	res.render('gestion_category_option', { err: '' ,local : req.query.local, session : req.session, category :  req.category});
});

/* POST gestion category option page. */
router.post('/category/:idCategory', function(req, res, next) {
	if (req.body.name != req.params.idCategory) {
		mongoose.model('category').findOne({name:req.body.name, local : req.query.local}, function (err, category){
			if(category == null){
				req.category.name = req.body.name;
				req.category.save()
				mongoose.model('product').update({category : req.params.idCategory, local : req.query.local},{$set : {category : req.body.name}},{multi: true},function(){});
				res.redirect('/gestion/category?local='+req.query.local);
			}
			else 
				next();//add err
		});
	}
	else 
		next()//add err
});

/* GET gestion category suprr. */
router.get('/category/:idCategory/suprr', function(req, res, next) {
	mongoose.model('category').remove({name : req.params.idCategory, local : req.query.local}, function (err){
		if (err) { throw err; }
		for (product of req.category.products){
			mongoose.model('product').findOneAndRemove({"_id" : product},function(err,product){
				if (product.image != '/images/banner-aius-small.png')
					fs.unlink('public'+product.image);
			});
		}
		res.redirect('/gestion/category?local='+req.query.local);
	});
});

/* GET gestion sellList page. */
router.get('/sellList', function(req, res, next) {
	date2 = new Date;
	date1 = new Date(date2.getTime() - date2.getDay()*24*60*60*1000);
	res.render('gestion_sellList', {local : req.query.local, date1 : date1,date2 : date2, session : req.session});
});

/* POST gestion sellList */
router.post('/sellList', function(req, res, next) {
	mongoose.model('vente').find({
		local : req.query.local,
		date : {
			$gte : req.body.date1,
			$lte : req.body.date2
		}
	})
	.sort({date : 1})
	.exec(function(err, ventes){
		if (err) { throw err; }
		res.send(JSON.stringify(ventes));
	});
});

module.exports = router;
