var express  = require('express'),
	router   = express.Router(),
	mongoose = require('../dbs').site,
	sha1     = require('sha1'),
	md5      = require('js-md5'),
	htmlEncode = require('../html_encode');

var err404 = new Error('Not Found');
err404.status	 = 404;

router.get('/',function(req,res,next) {
	mongoose.model('news').find().sort({date : -1}).limit(10).exec(function(err, news){
		res.render('index', { indexes: news , session : req.session});
	});
});

/* GET irc page. */
router.get('/irc', function(req, res, next) {
	if(req.session.isConnect)
		res.render('irc', { session : req.session });
	else
		next()
});

router.use('/about',function(req,res,next){
	mongoose.model('about').findOne({},function(err,about){
		req.about = about;
		next();
	});
});

/* GET about Us page. */
router.get('/about', function(req, res, next) {
	res.render('about', { about : req.about, session : req.session });
});

router.use('/about/edit',function(req, res, next) {
	if (req.session.account.groupe < 50)
		next(err404);
	else
		next();
});
/* GET about Us page edit. */
router.get('/about/edit', function(req, res, next) {
	res.render('about_edit', { about : req.about, session : req.session });
});

/* POST about Us page edit. */
router.post('/about/edit', function(req, res, next) {
	mongoose.model('about').update({},{$set :{contenu  : htmlEncode(req.body.contenu)}},function(err,about){
		console.log(req.body.contenu);
		res.redirect('/about');
	});
});

/* GET disconnect page*/
router.get('/disconnect', function (req , res, next){
	req.session.destroy();
	res.redirect('/');
});

/* GET connect page. */
router.get('/connect', function(req, res, next) {
	res.render('connect', { err : '' , session : req.session });
});


/* POST connect page */
router.post('/connect', function(req, res, next) {
		
	mongoose.model('account').findOne({ $or: [ { 'pseudo' : req.body.user},{ 'email': req.body.user}] , 'pass' : sha1(req.body.pass) },{pass : 0, posts : 0}, function(err, account){
		if (account == null)
			res.render('connect', { err : 'Le nom d\'utilisateur et/ou le mot de passe est/sont erroné(s).\n Veuillez vérifier et réessayer.' , session : req.session });
		else {
			req.session.isConnect = true;
			req.session.account = account;
			console.log(account.pseudo+' vient de se connecter');
			res.redirect('profil/'+account.pseudo);
		}
	});
});

/* GET register page. */
router.get('/register', function(req, res, next) {
	if (req.session.account.groupe >= 50)
		res.render('register', { err: '' , session : req.session});
	else
		next();
});

/* POST register page. */
router.post('/register',function(req, res, next) {
	if (req.session.account.groupe >= 50){
		var accountModel = mongoose.model('account');
		
		accountModel.findOne({ $or: [ { 'pseudo' : req.body.pseudo},{ 'email': req.body.email}] }, function(err,doc){
			if  (doc == null){
				if (( /^[a-zA-Z0-9-_]+$/ ).test(req.body.pseudo) == false)
					res.render('register', {
						err: 3,
						session : req.session
					});
				else if ((/^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/i).test(req.body.email) == false)
					res.render('register', {
						err: 4,
						session : req.session
					});
				/* PASSWORD test */ 
					else if (req.body.pass.length < 8)
						res.render('register', {
							err: 5,
							session : req.session
						});
					else if((/^(\w|¥|€|\$|£|@|%|&|§|#|\.|,|!|\?)+$/).test(req.body.pass) == false)
						res.render('register', {
							err: 6,
							session : req.session
						});
					else if ((/(¥|€|\$|£|@|%|&|§|#|\.|,|!|\?)+/).test(req.body.pass) == false) 
						res.render('register', {
							err: 7,
							session : req.session
						});
					else if((/\d+/).test(req.body.pass) == false )
						res.render('register', {
							err: 8,
							session : req.session
						});
				else {
					
					var account = new accountModel;
					account.pseudo = req.body.pseudo;
					account.email = req.body.email;
					account.pass = sha1(req.body.pass);
					account.imProfil = "http://www.gravatar.com/avatar/" + md5(req.body.email) +"?s=200&r=pg&f=y"
					
					account.save(function (err, account) {
						if (err) { throw err; }
						console.log(account.pseudo + ': compte enregistré avec succès !');
						res.redirect('profil/'+account.pseudo);
					});
					
				}
			}
			else if  (doc.pseudo == req.body.pseudo)
				res.render('register', { err: 0 , session : req.session }); //Nom d'utilisateur déja utilisé
			else if (doc.email == req.body.email)
				res.render('register', { err: 1 , session : req.session }); //Email déja utilisé
		});
	}
	else
		next();
});

router.use('/news/',require('./news'));

module.exports = router;
