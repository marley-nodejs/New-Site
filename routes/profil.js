var express = require('express');
var router = express.Router();
var mongoose = require('../dbs').site;
var sha1 = require('sha1');
var md5 = require('js-md5');
var multer = require('multer');
var fs = require('fs');

router.use(multer({
  dest: './public/memberImage/',
  rename: function (fieldname, filename) {
    return Date.now() + filename.replace(/\W+/g, '-').toLowerCase()
  }
}));

/* GET profil page. */
router.get('/:idAccount', function(req, res, next) {
	mongoose.model('account').findOne({ 'pseudo' : req.params.idAccount }, function (err, account){
		if (err) { throw err; }
		if (account==null)
			next();
		else 
			res.render('profil', { account: account , session : req.session });
	});
});

/* GET modified profil page*/
router.get('/:idAccount/modifier', function(req, res, next) {
	if(req.params.idAccount == req.session.account.pseudo) {
		mongoose.model('account').findOne({ 'pseudo' : req.params.idAccount}, function (err, account){
			if (err) { throw err; }
			if (account==null) 
				next();
			else 
				res.render('modified', { account: account , session : req.session });
		});
	}
	else {next()}
});


/* POST modified profil page*/
router.post('/:idAccount/modifier', function(req, res, next) {
	if(req.params.idAccount == req.session.account.pseudo) {
		mongoose.model('account').findOne({ 'pseudo' : req.params.idAccount}, function (err, account){
			if (err) { throw err; }
			if (account==null) 
				next();
			else {
				if (req.body.pass1 != req.body.pass2) 
					next();
				else{
					if (req.body.pass1 != undefined)
						account.pass = sha1(req.body.pass2);
					account.email = req.body.email;
					account.nom = req.body.nom;
					account.prenom = req.body.prenom;
					account.age = req.body.age;
					account.ville = req.body.ville;
					account.commentaire = req.body.commentaire;
					account.champDescriptif = req.body.champDescriptif;
			
					if (req.files.file1 != undefined) {
						fs.unlink('./public/'+account.imProfil, function(err){
							if (err) console.log(err);
							else console.log("supprimé");
						});
						account.imProfil = '/memberImage/'+req.files.file1.name;
					}
					if (req.files.file2 != undefined) {
						fs.unlink('./public/'+account.imCouv, function(err){
							if (err) console.log(err);
							else console.log("supprimé");
						});
						account.imCouv = '/memberImage/'+req.files.file2.name;
					}

			
					account.save();
					req.session.account = account;
					res.render('modified', { account: account , session : req.session });
				}
			}
		});
	}
	else next()
});

module.exports = router;
