var express = require('express');
var router = express.Router();
var mongoose = require('../dbs').wiki;
var sha1 = require('sha1');
var md5 = require('js-md5');
var htmlEncode = require('../html_encode');

router.use(function(req, res, next) {
	if (req.session.account.groupe >= 40)
		next();
	else
	{
		var err = new Error('Not Found');
		err.status	 = 404;
		next(err);
	}
});

router.get('/',function(req,res,next) {
	var pageModel = mongoose.model('page');
	pageModel.findOne({title : 'index'},function(err, page){
		res.render('wiki_page', { page: page , session : req.session});
	});
});

router.get('/add',function(req,res,next) {
	res.render('wiki_add', { session : req.session});
});

router.post('/add',function(req,res,next) {
	var pageModel = mongoose.model('page');
	pageModel.findOne({title : req.body.pageTitle},function(err, page){
		if(page == null){
			page = new pageModel;
			page.title = req.body.pageTitle;
			page.ownerId = req.session.account.pseudo;
			page.body = htmlEncode(req.body.pageContent);
			page.tags = req.body.pageTags.split(/\s*,\s*/);
			page.save();
			
			res.redirect('/wiki/'+req.body.pageTitle);
		}
		else				
			next();
	});
});

router.get('/search', function(req, res, next) {
	if (req.query.pageTag !== undefined){
		var pageModel = mongoose.model('page');
		var tagQuery = req.query.pageTag.split(/\s*,\s*/);
		
		pageModel.find({tags: {$elemMatch: {$in: tagQuery}}}, function(err, pages) {	
			res.render('wiki_search', {pages : pages, session : req.session	});
		});
	}
	else
		next();
});

router.get('/listePage',function(req,res,next) {
	var pageModel = mongoose.model('page');
	pageModel.find(null ,function(err, pages){
		res.render('wiki_listePage', { pages: pages , session : req.session});
	});
});

router.get('/:idPage',function(req,res,next) {
	var pageModel = mongoose.model('page');
	pageModel.findOne({title : req.params.idPage},function(err, page){
		if(page == null)
			next();
		else
			res.render('wiki_page', { page: page , session : req.session});
	});
});

router.get('/:idPage/edit',function(req,res,next) {
	var pageModel = mongoose.model('page');
	pageModel.findOne({title : req.params.idPage},function(err, page){
		if(page == null)
			next();
		else
			res.render('wiki_edit', { page: page , session : req.session});
	});
});

router.post('/:idPage/edit',function(req,res,next) {
	var pageModel = mongoose.model('page');
	pageModel.findOne({title : req.params.idPage},function(err, page){
		if(page == null)
			next();
		else {
			page.title = req.body.pageTitle;
			page.body = htmlEncode(req.body.pageContent);
			page.tags = req.body.pageTags.split(/\s*,\s*/);
			page.save();
			res.redirect('/wiki/'+req.body.pageTitle);
		}
	});
});

router.get('/:idPage/suppr',function(req,res,next) {
	var pageModel = mongoose.model('page');
	pageModel.remove({title : req.params.idPage},function(err, page){
		res.redirect('/wiki/');
	});
});



module.exports = router;
