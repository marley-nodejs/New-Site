function afficheDate(date)
{	
	var jour,nbJour,mois,annee,heure,mine;
	
	switch (date.getDay()){
		case 0 :
			jour="dimanche";
		break;
		case 1 :
			jour="lundi";
		break;
		case 2 :
			jour="mardi";
		break;
		case 3 :
			jour="mercredi";
		break;
		case 4 :
			jour="jeudi";
		break;
		case 5 :
			jour="vendredi";
		break;
		case 6 :
			jour="samedi";
		break;
	}
	nbJour = date.getDate();
	if (nbJour==1)
	{
		nbJour=nbJour+"er";
	}
	
	switch (date.getMonth()){
		case 0 :
			mois = "Janvier";
		break;
		case 1 :
			mois = "Février";
		break;
		case 2 :
			mois = "Mars";
		break;
		case 3 :
			mois = "Avril";
		break;
		case 4 :
			mois = "Mai";
		break;
		case 5 :
			mois = "Juin";
		break;
		case 6 :
			mois = "Juillet";
		break;
		case 7 :
			mois = "Août";
		break;
		case 8 :
			mois = "Septembre";
		break;
		case 9 :
			mois = "Octobre";
		break;
		case 10 :
			mois = "Novembre";
		break;
		case 11 :
			mois = "Décembre";
		break;
	}
	annee=date.getFullYear();
	heure=date.getHours();
	mine=date.getMinutes();
	if(mine < 10)
	{
		mine="0"+mine;
	}
	return "Le "+jour+" "+nbJour+" "+mois+" "+annee+" à "+heure+"h"+mine;
}
