var caisse = 0;
var listeProducts = [];
function enleve(produit){
	var id = (produit.category+produit.name).replace(/\s/,'-');
	
	caisse -= produit.prix;
	$('#compteur').text((caisse/100).toFixed(2).replace('.',','));
	
	nb = parseInt($('#'+id+'nb').text())-1;
	if(nb <= 0){
		listeProducts.splice(listeProducts.indexOf({name : produit.name,categorie : produit.categorie}),1);
		$('#liste'+id).remove();
	}
	else {
		$('#'+id+'nb').text(nb);
		$('#'+id+'total').text((produit.prix*nb/100).toFixed(2).replace('.',','));
	}
}
function ajoute(produit){
	var id = (produit.category+produit.name).replace(/\s/,'-');
	
	caisse += produit.prix;
	$('#compteur').text((caisse/100).toFixed(2).replace('.',',')+' €');
	
	if(($('#liste'+id)).length){
		nb = parseInt($('#'+id+'nb').text()) +1;
		$('#'+id+'nb').text(nb);
		$('#'+id+'total').text((produit.prix*nb/100).toFixed(2).replace('.',','));
	}
	else {
		listeProducts.push({name : produit.name,category : produit.category});
		$('<tr id="liste'+id+'">\
<td>'+produit.name+'</td><td> x <strong id='+id+'nb>1 </strong></td><td>  :   </td> \
<td id='+id+'total>'+(produit.prix/100).toFixed(2).replace('.',',')+'</td><td>€</td> \
<td><button class="btn-default" onclick="enleve({name : \''+produit.name+'\',category : \''+produit.category+'\' ,prix: '+produit.prix+'})">\
<span class="glyphicon glyphicon-minus"></span></button></td></tr>').appendTo($('#liste'));
	}
}

function confirm() {
	if(caisse > 0){
	$('#confirmVente').html('<div class="modal-dialog">\
    <div class="modal-content">\
      <div class="modal-header">\
        <h4 class="modal-title">Confirmation de la vente</h4>\
      </div>\
      <div class="modal-body">prix: '+(caisse/100).toFixed(2).replace('.',',') +' €\
      </div>\
      <div class="modal-footer">\
      <button class="btn btn-info" data-dismiss="modal">Annuler</button>\
      <button class="btn btn-info" onclick="envoi()">Confirmer</button>\
      </div>\
    </div>\
  </div>').modal('show');
	}
}

function envoi(){
	$('#confirmVente').modal('hide');
	var listeVente = [];
	while (listeProducts.length > 0){
		var produit = listeProducts.pop();
		var id = (produit.category+produit.name).replace(/\s/,'-');
		produit.nb = parseInt($('#'+id+'nb').text());
		listeVente.push(produit);
	}
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200 ) {
			$('#success').html('<div class="alert alert-block alert-success" ><h4>Success</h4>Vente enregistré.</div>').show("slow").delay(1000).hide("slow");
		}
	}
	xhr.open("POST", "", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	
	xhr.send("listeVente="+JSON.stringify(listeVente)+"&caisse="+caisse);
	$('#liste').replaceWith('<div id="liste"></div>');
	caisse = 0;
	$('#compteur').text('0 €');	
}

function searchVente(callback,date1,date2){
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200 ) {
			callback(JSON.parse(xhr.responseText));
		}
	}
	xhr.open("POST", "", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("date1="+date1.getTime()+"&date2="+(date2.getTime()+24*60*60*1000));
}

function afficheVente(ventes){
	var total = 0;
	var code = '';
	while (ventes.length > 0){
		var vente = ventes.pop();
		var date = new Date(vente.date);
		code = '</td><td>'+date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes()+'</td><td>'+vente.caisse/100+' €</td></tr>' + code
		while (vente.listeVente.length > 0 ){
			var produit = vente.listeVente.pop();
			code = produit.category+' <strong>'+produit.name+'</strong> '+produit.nb+'<br>' + code;
		}
		code = '<tr><td>'+vente.nomVendeur+'</td><td>'+code; 
	total += vente.caisse;
	}
	$('#listeVente').html(code);
	$('#total').text('Total : '+(total/100).toFixed(2).replace('.',',') +' €');
}

function listeVente(){
	$('#listeVente').replaceWith('<tbody id="listeVente"></tbody>');
	date1 = new Date($('#date1year').val(),$('#date1month').val()-1,$('#date1day').val());
	date2 = new Date($('#date2year').val(),$('#date2month').val()-1,$('#date2day').val());
	if (date1.getTime() <= (date2.getTime()+24*60*60*1000)) {   
		searchVente(function(liste){
			afficheVente(liste);
		},date1,date2);
	}
}
