$(function(){   
	var check = {};

	check['pseudo'] = function() {
		if (( /^[a-z0-9-_]+$/i ).test($('#pseudo').val())){
			changeTooltip('pseudo',true,'Nom d\'utilisateur correct');
			return true;
		}
		else {
		    changeTooltip('pseudo',false,'Nom d\'utilisateur non valide');
			return false;
		}
	};
	
	check['email'] = function() {
		if ((/^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/i).test(email.value)){
			changeTooltip('email',true,'Adresse email valide.');
			return true ;
		}
		else {
			changeTooltip('email',false,'Adresse email non valide.');
			return false;
		}
	};
	
	check['pass'] = function() {
		var pass = $("#pass").val();
		if ((/^(\w|¥|€|\$|£|@|%|&|§|#|\.|,|!|\?)+$/).test(pass)){
			if(pass.length >= 8){
				if ((/(¥|€|\$|£|@|%|&|§|#|\.|,|!|\?)+/).test(pass)) {
					if((/\d+/).test(pass) ){
						changeTooltip('pass',true,'Mot de passe valide');
						return true;
					}
					else 
						changeTooltip('pass',false,'Le mot de passe doit contenir au moins un chiffre');
				}
				else 
					changeTooltip('pass', false ,'Le mot de passe doit contenir au moins un caractère spécial (¥,€,$,£,@,%,&,§,".",",",?,! ou #)');
			}
			else 
				changeTooltip('pass',false,'Le mot de passe doit avoir au minimun 8 caractères.');
		}
		else 
			changeTooltip('pass',false ,'Le mot de passe peut contenir des lettres et doit contenir au moins un chiffre et un caractère spécial (¥,€,$,£,@,%,&,§,".",",",?,! ou #)');
		return false;
	};

	check['pass2'] = function() {
		var pass = $("#pass").val(), pass2 = $("#pass2").val();
		if (pass == pass2 && pass2 != '') {
			changeTooltip('pass2',true,'Les mots de passes sont identique.')
			return true;
		}
		else {
			changeTooltip('pass2',false,'Les mots de passes doivent être identique.')
			return false;
		}
	};

	for (var i in check)
		$('#'+i).keyup(check[i]).focus(check[i]);
	
	$('#formRegister').submit( function(e) {
		var result = true;
		
		for (var i in check)
			result = check[i]() && result;
		
		if (result == false) 
			e.preventDefault();
		
	});
	
	function changeTooltip(input, condition, message){
		var $input = $('#'+input),
			$tooltipDiv = $('#tooltip-'+input),
			$tooltip = $('#tooltip-'+input+' > .tooltip-inner'),
			
			p = $input.position();
		
		if (condition) {
			if ($tooltip.text() != message){
				$tooltip.removeClass("incorrect").addClass("correct").text(message);
				$tooltipDiv.css({
					top		: p.top -($tooltipDiv.height()/2) + $input.height()*3/4,
					left	: p.left + $input.width()+25,
					display	: 'block'
				});
			}
			
			$input.removeClass("incorrect").addClass("correct");
		
		} else {
			if ($tooltip.text() != message){
				$tooltip.removeClass("correct").addClass("incorrect").text(message);
				$tooltipDiv.css({
					top		: p.top -($tooltipDiv.height()/2) + $input.height()*3/4,
					left	: p.left + $input.width()+25,
					display	: 'block'
				});
			}
			
			$input.removeClass("correct").addClass("incorrect");
		
		}
	}
});
