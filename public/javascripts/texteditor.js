tinyMCE.init({
    mode : "textareas",
    language : "fr_FR",
    theme : "modern",
    formats : {
		bold : {inline : 'b'},
        italic : {inline : 'i'},
        underline : {inline : 'u'},
        strikethrough : {inline : 'del'},
	},
    style_formats: [
		{title: 'Titre', items: [
            {title: 'Titre1', format: "h1"},
            {title: 'Titre2', format: "h2"},
            {title: 'Titre3', format: "h3"},
        ]},
        {title: 'Style', items: [
			{title: 'Gras', icon: "bold", format:"bold"},
			{title: 'Italique',icon: "italic", format: "italic"},
			{title: 'Souligné',icon: "underline", format: "underline"},
			{title: 'Barée',icon: "strikethrough", format: "strikethrough"},
			{title: 'Exposant',icon: "superscript", format: "superscript"},
			{title: 'Indice', icon: "subscript", format: "subscript"},
		]},
		{title: "Alignment", items: [
			{title: "Gauche", icon: "alignleft", format: "alignleft"},
			{title: "Centrer", icon: "aligncenter", format: "aligncenter"},
			{title: "Droite", icon: "alignright", format: "alignright"},
			{title: "Justifier", icon: "alignjustify", format: "alignjustify"}
		]}
	],
    plugins : [
        "autolink link image anchor emoticons",
        "table paste imagetools aiusCode hr ",
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic underline hr | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image emoticons aiusCode | table",
	menubar: false
});
