function afficheMenu(){
	if ($('[name=menu]').val() == 'menu'){
		$('#commande').html('<div id="croc1"> Croc 1 : \
		<select name="pain1">\
		  <option value="nature" >Nature</option>\
		  <option value="complet" >Complet</option>\
		</select>\
		<select name="viande1">\
		  <option value="jambon" >Jambon</option>\
		  <option value="poulet" >Poulet</option>\
		</select>\
		<label> Tomate \
		  <input name="tomate1" type="checkbox" value="true"/>\
		</label>\
		</div>\
		<div id="croc2"> Croc 2 : \
		<select name="pain2">\
		  <option value="nature" >Nature</option>\
		  <option value="complet" >Complet</option>\
		</select>\
		<select name="viande2">\
		  <option value="jambon" >Jambon</option>\
		  <option value="poulet" >Poulet</option>\
		</select>\
		<label> Tomate \
		  <input name="tomate2" type="checkbox" value="true"/>\
		</label>\
		</div>\
		<div>12h<input name="heure" type="number"  min=0 max=59 value=0></div>');
		
		$('#prixCroc').text('2,30€');
	}
	else if ($('[name=menu]').val() == 'croc'){
		$('#commande').html('<select name="pain">\
		  <option value="nature" >Nature</option>\
		  <option value="complet" >Complet</option>\
		</select>\
		<select name="viande">\
		  <option value="jambon" >Jambon</option>\
		  <option value="poulet" >poulet</option>\
		  </select>\
		<label> Tomate \
		  <input name="tomate" type="checkbox" value="true"/>\
		</label>\
		<div>12h<input name="heure" type="number"  min=0 max=59 value=0></div>');
		$('#prixCroc').text('1,00€');
	}
	else {
		$('#commande').html('<label> Banane \
		  <input name="banane" type="checkbox" value="true"/>\
		</label><label> Poire \
		  <input name="poire" type="checkbox" value="true"/>\
		</label>');
		$('#prixCroc').text('0,50€');
	}
}

function envoiCroc() {
	commande ={};
	if ($('[name=nameCroc]').val() != '')
		commande.name = $('[name=nameCroc]').val();
	if ($('[name=menu]').val() == 'menu'){
		commande.menu = 0;
		commande.croc1 = {
			nature : $('[name=pain1]').val() == 'nature',
			jambon : $('[name=viande1]').val() == 'jambon',
			tomate : $('[name=tomate1]').is(':checked')
		};
		commande.croc2 = {
			nature : $('[name=pain2]').val() == 'nature',
			jambon : $('[name=viande2]').val() == 'jambon',
			tomate : $('[name=tomate2]').is(':checked')
		};
		commande.heure = $('[name=heure]').val();
	}
	else if ($('[name=menu]').val() == 'croc'){
		commande.menu = 1;
		commande.nature = $('[name=pain]').val() == 'nature', 
		commande.jambon = $('[name=viande]').val() == 'jambon';
		commande.tomate = $('[name=tomate]').is(':checked');
		commande.heure = $('[name=heure]').val();
	}
	else {
		commande.menu = 2;
		commande.banane = $('[name=banane]').is(':checked');
		commande.poire  = $('[name=poire]').is(':checked');
	}
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200 ) {
			if (document.location.pathname == '/gestion/reservation')
				rechargerListe();
			else
				$('#success').html('<div class="alert alert-block alert-success" ><h4>Success</h4>Reservation enregistré.</div>').show("slow").delay(2000).hide("slow");
		}
	}
	xhr.open("POST", "/reservation", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	commande = JSON.stringify(commande);
	xhr.send("commande="+commande);
	
}

function rechargerListe() {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200 ) {
			var reservs = JSON.parse(xhr.responseText);
			var codet = '';
			var nbSale = 0;
			var nbSucre = 0;
			while (reservs.length > 0){
				reserv = reservs.pop();
				var code = '';
				if(reserv.menu == 0){
					code = '<tr><td>'+reserv.acheteur+'</td><td>';
					if (reserv.croc1.nature)
						code += 'nature, ';
					else
						code += 'complet, ';
					
					if (reserv.croc1.jambon)
						code += 'jambon, ';
					else
						code += 'poulet, ';
					
					if (reserv.croc1.tomate)
						code += 'avec'
					else
						code += 'sans';
					code += ' tomates</td><td>';
					
					if (reserv.croc2.nature)
						code += 'nature, ';
					else
						code += 'complet, ';
					
					if (reserv.croc2.jambon)
						code += 'jambon ';
					else
						code += 'poulet ';
					
					if (reserv.croc2.tomate)
						code += 'avec'
					else
						code += 'sans';
					code += ' tomates</td><td>12h'+normaliseMinutes(reserv.heure)+'</td><td>2.30€</td><td>';
					nbSale += 2;
				}
				else if (reserv.menu == 1){
					code = '<tr><td>'+reserv.acheteur+'</td><td>';
					if (reserv.croc1.nature)
						code += 'nature, ';
					else
						code += 'complet, ';
					if (reserv.croc1.jambon)
						code += 'jambon, ';
					else
						code += 'poulet, ';
					if (reserv.croc1.tomate)
						code += 'avec'
					else
						code += 'sans';
					code += ' tomates</td><td></td><td>12h'+normaliseMinutes(reserv.heure)+'</td><td>1,00€</td><td>';
					nbSale++;
				}
				else {
					code = '<tr><td>'+reserv.acheteur+'</td><td>choco, ';
					
					if (reserv.croc1.tomate)
						code += 'avec'
					else
						code += 'sans';
					code += ' banane, ';
					if (reserv.croc2.tomate)
						code += 'avec'
					else
						code += 'sans';
					code += ' poire</td><td></td><td>13h00</td><td>0.50€</td><td>';
					nbSucre++;
				}
				if(reserv.payer){
					code += 'payer</td>';
					if(reserv.servi)
						code += '<td>servi</td>';
					else
						code += '<td><button onclick="servi(\''+reserv._id+'\')">servi</button></td>';
				}
				else
					code += '<button onclick="payer(\''+reserv._id+'\')">payer</button></td><td><button disabled>servi</button></td>';
				
				codet = code +'<td><button onclick="annuler(\''+reserv._id+'\')">annuler</button></td></tr>'+ codet;
			}
			$('#listeCrocs').html(codet);
			$('#sale').text(nbSale);
			$('#sucre').text(nbSucre);
		}
	}
	xhr.open("POST", "/gestion/reservation", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("");
}

function payer(id) {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			rechargerListe();
		}
	}
	xhr.open("GET","/gestion/reservation/payer?id="+id);
	xhr.send(null);
}
function servi(id) {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			rechargerListe();
		}
	}
	xhr.open("GET","/gestion/reservation/servi?id="+id);
	xhr.send(null);
}

function annuler(id) {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			rechargerListe();
		}
	}
	xhr.open("GET","/gestion/reservation/annuler?id="+id);
	xhr.send(null);
}

function prixCable() {
		var longeur = $('[name=longeur]').val();
		var prix ;
		if (longeur <=2) 
			prix = 150;
		else
			prix = 80*longeur;
		$('#prixCable').text(prix/100+'€');
}

function envoiCable() {
	var commande ={}
	commande.longeur=parseInt($('[name=longeur]').val());
	commande.croise = $('[name=croise]').is(':checked');
	if ($('[name=nameCable]').val() != '')
		commande.name = $('[name=nameCable]').val();
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			$('#success').html('<div class="alert alert-block alert-success" ><h4>Success</h4>Reservation enregistré.</div>').show("slow").delay(2000).hide("slow");
		}
	}
	xhr.open("POST","/reservation/cable/",true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	commande = JSON.stringify(commande);
	xhr.send("commande="+commande);
}

function rechargerListeCable() {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200 ) {
			var reservs = JSON.parse(xhr.responseText);
			var codet = '';
			while (reservs.length > 0){
				reserv = reservs.pop();
				var	code = '<tr><td>'+reserv.acheteur+'</td><td>'+reserv.longeur+'</td><td>'
				if(reserv.croise)
					code += 'Oui';
				else
					code += 'Non';
				code += '</td><td>'+parseInt(reserv.prix)/100+'€</td><td>';
				if(reserv.payer)
					code += 'payer</td>';
				else
					code += '<button onclick="payerCable(\''+reserv._id+'\')">payer</button></td>';
				if(reserv.pret)
					code += '<td>prêt</td>';
				else
					code += '<td><button onclick="pretCable(\''+reserv._id+'\')">prêt</button></td>';
				codet = code +'<td><button onclick="annulerCable(\''+reserv._id+'\')">annuler</button></td></tr>'+ codet;
			}
			$('#listeCable').html(codet);
		}
	}
	xhr.open("POST", "/gestion/reservation/cable", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("");
}

function payerCable(id) {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			rechargerListeCable();
		}
	}
	xhr.open("GET","/gestion/reservation/cable/payer?id="+id);
	xhr.send(null);
}
function pretCable(id) {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			rechargerListeCable();
		}
	}
	xhr.open("GET","/gestion/reservation/cable/pret?id="+id);
	xhr.send(null);
}

function annulerCable(id) {
	xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200 ) {
			rechargerListeCable();
		}
	}
	xhr.open("GET","/gestion/reservation/cable/annuler?id="+id);
	xhr.send(null);
}

function normaliseMinutes(minutes) {
	minutes = minutes%60;
	if (minutes < 10)
		minutes = '0'+minutes;
	return minutes;
}
