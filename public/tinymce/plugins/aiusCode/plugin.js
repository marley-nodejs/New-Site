tinymce.PluginManager.add('aiusCode', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('aiusCode', {
        icon: true,
        image: "/images/icon-code.png",
        tooltip: "Insérer du code", 
        onclick: function() {
            editor.windowManager.open({
                title: 'Code',
                body: [
                    {type: 'textbox', name: 'lang', label: 'langage du code :'}
                ],
                onsubmit: function(e) {
                    editor.insertContent('<pre class="brush: '+e.data.lang+';">Insérez votre code ici.\nPour sortir de la balise faite Shift+Entrée.</pre>');
                }
            });
        }
    });
});
