module.exports = function(html) {
	return html.replace(/<script>/g,'&ltscript&gt')
		.replace(/<\/script>/g,'&lt/script&gt')
		.replace(/<style>/g,'&lt/style&gt')
		.replace(/<\/style>/g,'&lt/style&gt')
		.replace(/\<pre class="brush: ([\s\S]*?);">([\s\S]*?)<\/pre>/g, function (match,p1,p2, offset, strings){
			return '<pre class="brush: '+p1+';">'+p2.replace(/(<(\/br>|br|br\/|br \/)>)|\r\n/g,'\n')+'</pre>';
		}).replace(/\r\n/g,'').replace(/'/g,"&#39;")
}
